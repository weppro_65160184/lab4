type Gender = 'male' | 'female' | 'others'
type roles = 'admin' | 'user'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  gender: Gender
  roles: roles[]
}

export type {Gender,roles,User}