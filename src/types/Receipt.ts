import type {User} from './User'
import type {Member} from './Member'
import type { ReceiptItem } from './ReceiptItem';

type Receipt = {
    id: number;
    createDate: Date;
    totalBefore:number,
    memberDiscount:number,
    total: number;
    receiveAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member?:Member
    receiptItems?:ReceiptItem[]
}

export type {Receipt}